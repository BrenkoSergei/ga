package by.bsuir.ga;

import by.bsuir.ga.model.City;
import by.bsuir.ga.model.Tsp;
import by.bsuir.ga.util.TspParser;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Scanner;
import java.util.function.Function;
import java.util.stream.Collectors;

public class BestResultCalculator {

    public static void main(String[] args) {
        File tspFile = new File("E:\\ga\\test problems\\berlin52.tsp");
        File tourFile = new File("E:\\ga\\test problems\\berlin52.opt.tour");

        Tsp parse = new TspParser().parse(tspFile);
        List<String> tour = parse(tourFile);

        Map<String, City> collect = parse.getCities().stream()
                .collect(Collectors.toMap(City::getName, Function.identity()));

        Double result = 0D;
        for (int i = 0; i < tour.size() - 1; i++) {
            City city1 = collect.get(tour.get(i));
            City city2 = collect.get(tour.get(i + 1));
            result += calculateDistance(city1, city2);
        }
        result += calculateDistance(collect.get(tour.get(0)), collect.get(tour.get(collect.size() - 1)));
        System.out.println(result);
    }

    private static double calculateDistance(City city1, City city2) {
        return Math.sqrt(
                Math.pow(city1.getX() - city2.getX(), 2)
                        + Math.pow(city1.getY() - city2.getY(), 2)
        );
    }

    private static List<String> parse(File file) {
        try (Scanner scanner = new Scanner(file)) {
            String name = getNextLineValue(scanner);
            String comment = getNextLineValue(scanner);
//            String type = getNextLineValue(scanner);
            String dimension = getNextLineValue(scanner);
            scanner.nextLine();

            List<String> ids = new ArrayList<>();
            while (true) {
                String line = scanner.nextLine();
                if (!line.equalsIgnoreCase("-1")) {
                    ids.add(line);
                } else {
                    break;
                }
            }

            return ids;
        } catch (FileNotFoundException e) {
            e.printStackTrace();
            return null;
        }
    }

    private static String getNextLineValue(Scanner scanner) {
        return scanner.nextLine().split(":")[1].trim();
    }
}

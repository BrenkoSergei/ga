package by.bsuir.ga.util;

import com.alee.laf.button.WebButton;

public class Clicker extends Thread {

    private static final long INTERVAL = 50;

    private volatile boolean state;
    private final WebButton button;

    public Clicker(WebButton button) {
        setDaemon(true);
        this.button = button;
    }

    public void setState(boolean state) {
        this.state = state;
    }

    @Override
    public void run() {
        while (true) {
            if (state) {
                button.doClick(1);
            }
            sleep();
        }
    }

    private void sleep() {
        try {
            Thread.sleep(INTERVAL);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }
}

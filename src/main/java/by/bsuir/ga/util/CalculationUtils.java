package by.bsuir.ga.util;

import java.util.List;

public final class CalculationUtils {

    private CalculationUtils() {
    }

    public static double toReal(long x, int minValue, int maxValue, int numberOfGens) {
        return minValue + x * (maxValue - minValue) / (Math.pow(2, numberOfGens) - 1);
    }

    public static long getBeforeCrossPoint(long x, int numberOfGens, int crossPoint) {
        int shift = numberOfGens - crossPoint - 1;
        return (x >> shift) << shift;
    }

    public static long getAfterCrossPoint(long x, int numberOfGens, int crossPoint) {
        return x & (1 << (numberOfGens - crossPoint - 1)) - 1;
    }

    public static Double calculateRadius(List<BiPair<Double>> o) {
        BiPair<Double> point = o.iterator().next();
        return Math.sqrt(Math.pow(point.getFirst(), 2) + Math.pow(point.getSecond(), 2));
    }
}

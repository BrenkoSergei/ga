package by.bsuir.ga.util;

import by.bsuir.ga.model.City;
import by.bsuir.ga.model.Tsp;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.Scanner;

public class TspParser {

    public Tsp parse(File file) {
        try (Scanner scanner = new Scanner(file)) {
            String name = getNextLineValue(scanner);
            String comment = getNextLineValue(scanner);
            String type = getNextLineValue(scanner);
            String dimension = getNextLineValue(scanner);
            String edgeWeightType = getNextLineValue(scanner);
            scanner.nextLine();

            Tsp tsp = new Tsp(name, comment, type, Integer.parseInt(dimension), edgeWeightType);
            String line;
            while (!(line = scanner.nextLine()).equalsIgnoreCase("EOF")) {
                tsp.addCity(parseCity(line));
            }

            return tsp;
        } catch (FileNotFoundException e) {
            e.printStackTrace();
            return null;
        }
    }

    private String getNextLineValue(Scanner scanner) {
        return scanner.nextLine().split(":")[1].trim();
    }

    private City parseCity(String line) {
        String[] parts = line.split(" ");
        return new City(parts[0], Double.parseDouble(parts[1]), Double.parseDouble(parts[2]));
    }
}

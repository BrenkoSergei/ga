package by.bsuir.ga.util;

public class BiPair<T> extends Pair<T, T> {

    public BiPair(T first, T second) {
        super(first, second);
    }
}

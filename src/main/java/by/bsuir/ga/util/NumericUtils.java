package by.bsuir.ga.util;

public final class NumericUtils {

    private static final String DOUBLE_FORMAT = "%2.4f";

    private NumericUtils() {
    }

    public static String toString(double value) {
        return value < 0
                ? String.format(DOUBLE_FORMAT, value)
                : String.format(" " + DOUBLE_FORMAT, value);
    }
}

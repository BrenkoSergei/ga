package by.bsuir.ga.util;

import java.awt.*;
import java.util.ArrayList;
import java.util.List;

public abstract class ColorUtils {

    private static final List<Color> COLORS = new ArrayList<>();

    static {
        int r = 255;
        int g = 0;
        int b = 0;

        COLORS.add(new Color(r, g, b));
        for (int i = 0; i < 255; i++) {
            COLORS.add(new Color(r, ++g, b));
        }
        for (int i = 0; i < 255; i++) {
            COLORS.add(new Color(--r, g, b));
        }
        for (int i = 0; i < 255; i++) {
            COLORS.add(new Color(r, g, ++b));
        }
        for (int i = 0; i < 255; i++) {
            COLORS.add(new Color(r, --g, b));
        }
        for (int i = 0; i < 255; i++) {
            COLORS.add(new Color(++r, g, b));
        }
    }

    private ColorUtils() {
    }

    public static List<Color> getDistributedColors(int count) {
        int step = COLORS.size() / count;

        List<Color> result = new ArrayList<>();
        for (int i = 0; i < COLORS.size(); i += step) {
            result.add(COLORS.get(i));
        }

        return result;
    }
}

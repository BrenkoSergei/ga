package by.bsuir.ga.calculator;

import by.bsuir.ga.model.Chromosome;
import by.bsuir.ga.model.Context;

import java.util.Comparator;
import java.util.List;

public class DefaultCalculator<RX, AX> implements Calculator<RX, AX> {

    @Override
    public void calculate(Context<RX, AX> context) {
        List<Chromosome<RX, AX>> chromosomes = context.getChromosomes();

        preCalculation(context);

        Chromosome<RX, AX> bestChromosome = getBest(chromosomes);
        context.addBestChromosome(bestChromosome);

        double averageValue = getAverage(chromosomes);
        context.addAverageValue(averageValue);
    }

    void preCalculation(Context<RX, AX> context) {
    }

    Chromosome<RX, AX> getBest(List<Chromosome<RX, AX>> chromosomes) {
        return chromosomes.stream()
                .max(Comparator.comparing(Chromosome::getActualY))
                .orElse(null);
    }

    private double getAverage(List<Chromosome<RX, AX>> chromosomes) {
        return chromosomes.stream()
                .mapToDouble(Chromosome::getActualY)
                .average()
                .orElse(0);
    }
}

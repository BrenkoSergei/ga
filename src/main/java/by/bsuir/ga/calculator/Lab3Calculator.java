package by.bsuir.ga.calculator;

import by.bsuir.ga.model.Chromosome;
import by.bsuir.ga.model.City;
import by.bsuir.ga.model.Context;

import java.util.Comparator;
import java.util.List;

public class Lab3Calculator extends DefaultCalculator<Object, List<City>> {

    @Override
    void preCalculation(Context<Object, List<City>> context) {
        context.getChromosomes()
                .forEach(chromosome -> chromosome.setActualY(context.getFunction().calculate(chromosome.getActualX())));
    }

    @Override
    Chromosome<Object, List<City>> getBest(List<Chromosome<Object, List<City>>> chromosomes) {
        return chromosomes.stream()
                .min(Comparator.comparing(Chromosome::getActualY))
                .orElse(null);
    }
}

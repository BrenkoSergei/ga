package by.bsuir.ga.calculator;

import by.bsuir.ga.model.Chromosome;
import by.bsuir.ga.util.BiPair;

import java.util.Comparator;
import java.util.List;

public class Lab2Calculator extends DefaultCalculator<BiPair<Long>, BiPair<Double>> {

    @Override
    Chromosome<BiPair<Long>, BiPair<Double>> getBest(List<Chromosome<BiPair<Long>, BiPair<Double>>> chromosomes) {
        return chromosomes.stream()
                .min(Comparator.comparing(Chromosome::getActualY))
                .orElse(null);
    }
}

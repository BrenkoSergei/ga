package by.bsuir.ga.calculator;

import by.bsuir.ga.model.Context;

public interface Calculator<RX, AX> {

    void calculate(Context<RX, AX> context);
}

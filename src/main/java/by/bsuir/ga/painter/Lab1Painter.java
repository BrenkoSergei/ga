package by.bsuir.ga.painter;

import by.bsuir.ga.model.Chromosome;
import by.bsuir.ga.model.Context;
import org.jfree.data.xy.DefaultXYDataset;

import java.util.List;

public class Lab1Painter extends BasePainter<Long, Double> {

    @Override
    void repaintChromosomes(Context<Long, Double> context) {
        List<Chromosome<Long, Double>> chromosomes = context.getChromosomes();

        double[] xValues = chromosomes.stream()
                .mapToDouble(Chromosome::getActualX)
                .toArray();
        double[] yValues = chromosomes.stream()
                .mapToDouble(Chromosome::getActualY)
                .toArray();

        DefaultXYDataset dataset = new DefaultXYDataset();
        dataset.addSeries(CHROMOSOMES_SERIES_KEY, new double[][]{xValues, yValues});

        context.getChart().getXYPlot().setDataset(1, dataset);
    }
}

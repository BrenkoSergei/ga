package by.bsuir.ga.painter;

import by.bsuir.ga.model.Chromosome;
import by.bsuir.ga.model.Context;
import by.bsuir.ga.util.BiPair;
import org.jfree.data.xy.XYSeries;
import org.jfree.data.xy.XYSeriesCollection;

public class Lab2Painter extends BasePainter<BiPair<Long>, BiPair<Double>> {

    @Override
    void repaintChromosomes(Context<BiPair<Long>, BiPair<Double>> context) {
        XYSeriesCollection dataset = new XYSeriesCollection();
        XYSeries xySeries = new XYSeries(CHROMOSOMES_SERIES_KEY);
        dataset.addSeries(xySeries);

        context.getChromosomes().stream()
                .map(Chromosome::getActualX)
                .forEach(doublePair -> xySeries.add(doublePair.getFirst(), doublePair.getSecond()));

        context.getChart().getXYPlot().setDataset(0, dataset);
    }
}

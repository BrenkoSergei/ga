package by.bsuir.ga.painter;

import by.bsuir.ga.model.Context;
import org.jfree.data.Range;
import org.jfree.data.xy.DefaultXYDataset;

import java.util.List;
import java.util.stream.DoubleStream;

public abstract class BasePainter<RX, AX> implements Painter<RX, AX> {

    static final String CHROMOSOMES_SERIES_KEY = "Chromosomes";
    private static final String BEST_VALUES_SERIES_KEY = "Best Values";
    private static final String AVERAGE_VALUES_SERIES_KEY = "Average Values";

    private static final int DYNAMIC_CHART_MIN_X = 5;

    @Override
    public void paint(Context<RX, AX> context) {
        repaintChromosomes(context);
        repaintBestValues(context);
        repaintAverageValues(context);
    }

    abstract void repaintChromosomes(Context<RX, AX> context);

    private void repaintBestValues(Context<RX, AX> context) {
        List<Double> bestValues = context.getBestValues();
        double[] yValues = bestValues.stream()
                .skip(Math.max(bestValues.size() - 1000, 0))
                .mapToDouble(Double::doubleValue)
                .toArray();
        double[] xValues = DoubleStream.iterate(1, d -> d + 1)
                .skip(Math.max(bestValues.size() - 1000, 0))
                .limit(yValues.length)
                .toArray();

        DefaultXYDataset dataset = new DefaultXYDataset();
        dataset.addSeries(BEST_VALUES_SERIES_KEY, new double[][]{xValues, yValues});

        context.getExtraChart().getXYPlot().setDataset(0, dataset);
        context.getExtraChart().getXYPlot().getDomainAxis().setRange(getXAxisRange(Math.max(bestValues.size() - 1000, 0), xValues[xValues.length - 1]));
        context.getExtraChart().getXYPlot().getDomainAxis().setMinorTickCount((int) xValues[xValues.length - 1]);
    }

    private Range getXAxisRange(double start, double xValue) {
        return new Range(start, Math.max(xValue + 1, DYNAMIC_CHART_MIN_X));
    }

    private void repaintAverageValues(Context<RX, AX> context) {
        List<Double> averageValues = context.getAverageValues();
        double[] yValues = averageValues.stream()
                .skip(Math.max(averageValues.size() - 1000, 0))
                .mapToDouble(Double::doubleValue)
                .toArray();
        double[] xValues = DoubleStream.iterate(1, d -> d + 1)
                .skip(Math.max(averageValues.size() - 1000, 0))
                .limit(yValues.length)
                .toArray();

        DefaultXYDataset dataset = new DefaultXYDataset();
        dataset.addSeries(AVERAGE_VALUES_SERIES_KEY, new double[][]{xValues, yValues});

        context.getExtraChart().getXYPlot().setDataset(1, dataset);
    }
}

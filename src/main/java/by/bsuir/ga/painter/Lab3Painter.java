package by.bsuir.ga.painter;

import by.bsuir.ga.model.City;
import by.bsuir.ga.model.Context;
import org.jfree.data.xy.DefaultXYDataset;

import java.util.List;
import java.util.stream.Stream;

public class Lab3Painter extends BasePainter<Object, List<City>> {

    @Override
    void repaintChromosomes(Context<Object, List<City>> context) {
        List<City> bestTour = context.getCurrentBestValue().getActualX();
        double[] xes = Stream.concat(bestTour.stream(), Stream.of(bestTour.get(0))).mapToDouble(City::getX).toArray();
        double[] yes = Stream.concat(bestTour.stream(), Stream.of(bestTour.get(0))).mapToDouble(City::getY).toArray();

        DefaultXYDataset dataset = new DefaultXYDataset();
        dataset.addSeries("Tour", new double[][]{xes, yes});
        context.getChart().getXYPlot().setDataset(1, dataset);

        String[] cityNames = bestTour.stream().map(City::getName).toArray(String[]::new);
        context.setBestTourList(cityNames);
    }
}

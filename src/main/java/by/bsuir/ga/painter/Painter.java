package by.bsuir.ga.painter;

import by.bsuir.ga.model.Context;

public interface Painter<RX, AX> {

    void paint(Context<RX, AX> context);
}

package by.bsuir.ga.ui;

import by.bsuir.ga.model.Context;
import by.bsuir.ga.ui.callback.generate.Lab2GenerateButtonCallback;
import by.bsuir.ga.ui.callback.next.Lab2NextStepCallback;
import by.bsuir.ga.util.BiPair;
import by.bsuir.ga.util.Clicker;
import by.bsuir.ga.util.ColorUtils;
import com.alee.extended.button.WebSwitch;
import com.alee.extended.layout.TableLayout;
import com.alee.laf.button.WebButton;
import com.alee.laf.label.WebLabel;
import com.alee.laf.panel.WebPanel;
import com.alee.laf.spinner.WebSpinner;
import org.jfree.chart.ChartFactory;
import org.jfree.chart.ChartPanel;
import org.jfree.chart.JFreeChart;
import org.jfree.chart.annotations.XYShapeAnnotation;
import org.jfree.chart.plot.PlotOrientation;
import org.jfree.chart.plot.XYPlot;
import org.jfree.chart.renderer.xy.XYLineAndShapeRenderer;
import org.jfree.chart.ui.Layer;
import org.jfree.data.xy.DefaultXYDataset;
import org.jfree.data.xy.XYSeriesCollection;

import javax.swing.*;
import java.awt.*;
import java.awt.geom.Ellipse2D;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.TreeSet;
import java.util.stream.Collectors;

public class Lab2Tab extends WebPanel {

    private static final String MAIN_CHART_TITLE = "y = (x1*x1 + x2*x2)^0.25 * (sin(50(x1*x1 + x2*x2)^0.1)^2 + 1), xi [-100, 100]";
    private static final String DYNAMIC_CHART_TITLE = "Динамика лучшего и среднего значений";
    private static final BasicStroke LEVEL_LINE_STROKE = new BasicStroke(1.5f);

    private static final int MAX_POPULATION = 100;
    private static final int POPULATION_DEFAULT_VALUE = 10;
    private static final int GENS_DEFAULT_VALUE = 10;
    private static final double CROSSINGOVER_DEFAULT_VALUE = 0.8;
    private static final double MUTATION_DEFAULT_VALUE = 0.1;

    private final Context<BiPair<Long>, BiPair<Double>> context;

    public Lab2Tab(Context<BiPair<Long>, BiPair<Double>> context) {
        this.context = context;

        this.setLayout(new TableLayout(new double[][]{
                {TableLayout.FILL, TableLayout.PREFERRED},
                {TableLayout.FILL}
        }));
        this.add(getLeftPanel(), "0,0");
        this.add(getRightPanel(), "1,0");
    }

    private WebPanel getLeftPanel() {
        WebPanel leftPanel = new WebPanel();
        leftPanel.setLayout(new TableLayout(new double[][]{
                {TableLayout.FILL},
                {TableLayout.FILL, TableLayout.FILL, TableLayout.FILL}
        }));

        leftPanel.add(getMainChartPanel(), "0,0,0,1");
        leftPanel.add(getDynamicChartPanel(), "0,2");

        return leftPanel;
    }

    private ChartPanel getMainChartPanel() {
        TreeSet<Double> radiuses = calculateRadiusOfLevelLines();

        JFreeChart chart = ChartFactory.createScatterPlot(
                MAIN_CHART_TITLE, "X1", "X2", new XYSeriesCollection(),
                PlotOrientation.VERTICAL, false, true, false);
        XYPlot plot = chart.getXYPlot();
        plot.getDomainAxis().setRange(context.getMinValue() - 1, context.getMaxValue() + 1);
        plot.getRangeAxis().setRange(context.getMinValue() - 1, context.getMaxValue() + 1);
        XYLineAndShapeRenderer renderer = (XYLineAndShapeRenderer) plot.getRenderer();

        Iterator<Color> colors = ColorUtils.getDistributedColors(radiuses.size()).iterator();
        radiuses.forEach(radius -> {
            Ellipse2D.Double circle = new Ellipse2D.Double(-radius, -radius, radius * 2, radius * 2);
            XYShapeAnnotation annotation = new XYShapeAnnotation(circle, LEVEL_LINE_STROKE, colors.next());
            renderer.addAnnotation(annotation, Layer.BACKGROUND);
        });

        context.setChart(chart);
        return new ChartPanel(chart);
    }

    private TreeSet<Double> calculateRadiusOfLevelLines() {
        List<BiPair<Double>> results = new ArrayList<>();
        results.add(new BiPair<>(-100D, -100D));
        double step = 0.1;

        boolean isUp = true;
        double previousResult = 0;
        for (double i = 0; i <= 200; i += step) {
            BiPair<Double> doublePair = new BiPair<>(i, 0D);
            double result = context.getFunction().calculate(doublePair);

            if (!isUp && previousResult < result) {
                if (results.get(results.size() - 1).getSecond() < result) {
                    if (results.get(results.size() - 1).getFirst() < i) {
                        results.add(new BiPair<>(i, result));
                    }
                }
            }
            isUp = previousResult < result;
            previousResult = result;
        }
        results.remove(0);
        return results.stream()
                .map(BiPair<Double>::getFirst)
                .collect(Collectors.toCollection(TreeSet::new));
    }

    private ChartPanel getDynamicChartPanel() {
        DefaultXYDataset dataset = new DefaultXYDataset();
        JFreeChart chart = ChartFactory.createXYLineChart(
                DYNAMIC_CHART_TITLE, "Шаг", "F(X)", dataset, PlotOrientation.VERTICAL, true, true, false
        );
        context.setExtraChart(chart);

        XYLineAndShapeRenderer renderer1 = new XYLineAndShapeRenderer(true, true);
        XYLineAndShapeRenderer renderer2 = new XYLineAndShapeRenderer(true, true);

        context.getExtraChart().getXYPlot().setRenderer(0, renderer1);
        context.getExtraChart().getXYPlot().setRenderer(1, renderer2);
        context.getExtraChart().getXYPlot().setDataset(0, new DefaultXYDataset());
        context.getExtraChart().getXYPlot().setDataset(1, new DefaultXYDataset());
        context.getExtraChart().getXYPlot().getDomainAxis().setRange(0, 5);

        return new ChartPanel(chart);
    }

    private WebPanel getRightPanel() {
        WebPanel rightPanel = new WebPanel();
        rightPanel.setMargin(20);
        rightPanel.setLayout(new TableLayout(new double[][]{
                {TableLayout.PREFERRED, TableLayout.PREFERRED},
                {TableLayout.PREFERRED, TableLayout.PREFERRED, TableLayout.PREFERRED, TableLayout.PREFERRED,
                        TableLayout.PREFERRED, TableLayout.PREFERRED, TableLayout.PREFERRED, TableLayout.PREFERRED}
        }));

        WebButton nextStepButton = getNextStepButton();

        rightPanel.add(getPopulationLabel(), "0,0");
        WebSpinner populationSpinner = getPopulationSpinner();
        rightPanel.add(populationSpinner, "1,0");
        rightPanel.add(getGensLabel(), "0,1");
        rightPanel.add(getGensSpinner(nextStepButton, ((SpinnerNumberModel) populationSpinner.getModel())), "1,1");
        rightPanel.add(getCrossingoverLabel(), "0,2");
        rightPanel.add(getCrossingoverSpinner(), "1,2");
        rightPanel.add(getMutationLabel(), "0,3");
        rightPanel.add(getMutationSpinner(), "1,3");

        rightPanel.add(getGenerateButton(nextStepButton), "0,4,1,4");
        rightPanel.add(nextStepButton, "0,5,1,5");
        rightPanel.add(getAutoSwitcher(nextStepButton), "0,6,1,6");
        rightPanel.add(getStatisticInfoLabel(), "0,7,1,7");

        return rightPanel;
    }

    private WebPanel getAutoSwitcher(WebButton nextStepButton) {
        Clicker clicker = new Clicker(nextStepButton);
        clicker.start();

        WebSwitch webSwitch = new WebSwitch();
        webSwitch.setSwitchComponents("Авто", "Ручной");
        webSwitch.addActionListener(e -> clicker.setState(webSwitch.isSelected()));
        return new WebPanel(webSwitch);
    }

    private WebLabel getPopulationLabel() {
        return new WebLabel("Размер популяции: ");
    }

    private WebSpinner getPopulationSpinner() {
        SpinnerNumberModel numberModel = new SpinnerNumberModel(POPULATION_DEFAULT_VALUE, 4, MAX_POPULATION, 2);
        context.setPopulation(POPULATION_DEFAULT_VALUE);

        WebSpinner spinner = new WebSpinner();
        spinner.addChangeListener(e -> context.setPopulation(Integer.parseInt(spinner.getValue().toString())));
        spinner.setModel(numberModel);
        return spinner;
    }

    private WebLabel getGensLabel() {
        return new WebLabel("Количество генов: ");
    }

    private WebSpinner getGensSpinner(WebButton nextStepButton, SpinnerNumberModel populationSpinnerModel) {
        SpinnerNumberModel numberModel = new SpinnerNumberModel(GENS_DEFAULT_VALUE, 4, 40, 1);
        context.setNumberOfGens(GENS_DEFAULT_VALUE);

        WebSpinner spinner = new WebSpinner();
        spinner.addChangeListener(e -> context.setNumberOfGens(Integer.parseInt(spinner.getValue().toString())));
        spinner.addChangeListener(e -> nextStepButton.setEnabled(false));
        spinner.addChangeListener(e -> {
            int spinnerValue = numberModel.getNumber().intValue();
            if (spinnerValue < 10) {
                int maxPopulation = 2 << spinnerValue - 1;
                int population = Math.min(populationSpinnerModel.getNumber().intValue(), maxPopulation);
                context.setPopulation(population);
                populationSpinnerModel.setValue(population);
                populationSpinnerModel.setMaximum(Math.min(MAX_POPULATION, maxPopulation));
            }
        });
        spinner.setModel(numberModel);
        return spinner;
    }

    private WebLabel getCrossingoverLabel() {
        return new WebLabel("Вероятность кроссинговера: ");
    }

    private WebSpinner getCrossingoverSpinner() {
        SpinnerNumberModel numberModel = new SpinnerNumberModel(CROSSINGOVER_DEFAULT_VALUE, 0.4, 1, 0.1);
        context.setCrossingoverChance(CROSSINGOVER_DEFAULT_VALUE);

        WebSpinner spinner = new WebSpinner();
        spinner.addChangeListener(e -> context.setCrossingoverChance(Double.parseDouble(spinner.getValue().toString())));
        spinner.setModel(numberModel);
        return spinner;
    }

    private WebLabel getMutationLabel() {
        return new WebLabel("Вероятность мутации: ");
    }

    private WebSpinner getMutationSpinner() {
        SpinnerNumberModel numberModel = new SpinnerNumberModel(MUTATION_DEFAULT_VALUE, 0, 0.6, 0.05);
        context.setMutationChance(MUTATION_DEFAULT_VALUE);

        WebSpinner spinner = new WebSpinner();
        spinner.addChangeListener(e -> context.setMutationChance(Double.parseDouble(spinner.getValue().toString())));
        spinner.setModel(numberModel);
        return spinner;
    }

    private WebButton getGenerateButton(WebButton nextStepButton) {
        WebButton button = new WebButton("Обновить");
        button.addActionListener(new Lab2GenerateButtonCallback(context));
        button.addActionListener(e -> nextStepButton.setEnabled(true));
        button.addActionListener(e -> {
            context.getChart().getXYPlot().getDomainAxis().setRange(context.getMinValue() - 1, context.getMaxValue() + 1);
            context.getChart().getXYPlot().getRangeAxis().setRange(context.getMinValue() - 1, context.getMaxValue() + 1);
        });
        button.setPadding(5);
        button.setMargin(5);
        return button;
    }

    private WebButton getNextStepButton() {
        WebButton button = new WebButton("Следующий шаг");
        button.addActionListener(new Lab2NextStepCallback(context));
        button.setEnabled(false);
        button.setPadding(5);
        button.setMargin(5);
        return button;
    }

    private WebPanel getStatisticInfoLabel() {
        WebPanel panel = new WebPanel();
        panel.setMargin(20, 0, 0, 0);
        panel.setLayout(new TableLayout(new double[][]{
                {TableLayout.PREFERRED, TableLayout.PREFERRED},
                {TableLayout.FILL, TableLayout.FILL, TableLayout.FILL}
        }));

        panel.add(new WebLabel("Лучшая особь").changeFontSize(6), "0,0,1,0");
        panel.add(new WebLabel("Значение особи:"), "0,1");
        panel.add(getBestXValueLabel(), "1,1");
        panel.add(new WebLabel("Значение функции:"), "0,2");
        panel.add(getBestYValueLabel(), "1,2");

        return panel;
    }

    private WebLabel getBestXValueLabel() {
        WebLabel webLabel = new WebLabel("");
        webLabel.setPadding(0, 15, 0, 0);
        context.setBestXLabel(webLabel);
        return webLabel;
    }

    private WebLabel getBestYValueLabel() {
        WebLabel webLabel = new WebLabel("");
        context.setBestYLabel(webLabel);
        webLabel.setPadding(0, 15, 0, 0);
        return webLabel;
    }
}

package by.bsuir.ga.ui;

import by.bsuir.ga.model.City;
import by.bsuir.ga.model.Context;
import by.bsuir.ga.ui.callback.file.FileChooserActionListener;
import by.bsuir.ga.ui.callback.generate.Lab3GenerateButtonCallback;
import by.bsuir.ga.ui.callback.next.Lab3NextStepCallback;
import by.bsuir.ga.util.Clicker;
import com.alee.extended.button.WebSwitch;
import com.alee.extended.layout.TableLayout;
import com.alee.laf.button.WebButton;
import com.alee.laf.label.WebLabel;
import com.alee.laf.list.WebList;
import com.alee.laf.panel.WebPanel;
import com.alee.laf.scroll.WebScrollPane;
import com.alee.laf.spinner.WebSpinner;
import org.jfree.chart.ChartFactory;
import org.jfree.chart.ChartPanel;
import org.jfree.chart.JFreeChart;
import org.jfree.chart.plot.PlotOrientation;
import org.jfree.chart.renderer.xy.XYLineAndShapeRenderer;
import org.jfree.data.xy.DefaultXYDataset;

import javax.swing.*;
import java.util.List;

public class Lab3Tab extends WebPanel {

    private static final String DYNAMIC_CHART_TITLE = "Динамика лучшего и среднего значений";

    private static final int MAX_POPULATION = 2000;
    private static final int POPULATION_DEFAULT_VALUE = 50;
    private static final double CROSSINGOVER_DEFAULT_VALUE = 1;
    private static final double MUTATION_DEFAULT_VALUE = 0.2;

    private final Context<Object, List<City>> context;

    public Lab3Tab(Context<Object, List<City>> context) {
        this.context = context;

        this.setLayout(new TableLayout(new double[][]{
                {TableLayout.FILL, TableLayout.PREFERRED, TableLayout.PREFERRED},
                {TableLayout.FILL}
        }));
        this.add(getLeftPanel(), "0,0");
        this.add(getCenterPanel(), "1,0");
        this.add(getRightPanel(), "2,0");
    }

    private WebPanel getLeftPanel() {
        WebPanel leftPanel = new WebPanel();
        leftPanel.setLayout(new TableLayout(new double[][]{
                {TableLayout.FILL},
                {TableLayout.FILL, TableLayout.FILL, TableLayout.FILL}
        }));

        leftPanel.add(getMainChartPanel(), "0,0,0,1");
        leftPanel.add(getDynamicChartPanel(), "0,2");

        return leftPanel;
    }

    private ChartPanel getMainChartPanel() {
        JFreeChart citiesChart = ChartFactory.createScatterPlot("Cities", "", "", new DefaultXYDataset());

        citiesChart.getXYPlot().setRenderer(1, new XYLineAndShapeRenderer());
        citiesChart.getXYPlot().setDataset(1, new DefaultXYDataset());

        context.setChart(citiesChart);

        return new ChartPanel(citiesChart);
    }

    private ChartPanel getDynamicChartPanel() {
        DefaultXYDataset dataset = new DefaultXYDataset();
        JFreeChart chart = ChartFactory.createXYLineChart(
                DYNAMIC_CHART_TITLE, "Шаг", "F(X)", dataset, PlotOrientation.VERTICAL, true, true, false
        );
        context.setExtraChart(chart);

        XYLineAndShapeRenderer renderer1 = new XYLineAndShapeRenderer(true, true);
        XYLineAndShapeRenderer renderer2 = new XYLineAndShapeRenderer(true, true);

        context.getExtraChart().getXYPlot().setRenderer(0, renderer1);
        context.getExtraChart().getXYPlot().setRenderer(1, renderer2);
        context.getExtraChart().getXYPlot().setDataset(0, new DefaultXYDataset());
        context.getExtraChart().getXYPlot().setDataset(1, new DefaultXYDataset());
        context.getExtraChart().getXYPlot().getDomainAxis().setRange(0, 5);

        return new ChartPanel(chart);
    }

    private WebPanel getCenterPanel() {
        WebPanel rightPanel = new WebPanel();
        rightPanel.setMargin(20);
        rightPanel.setLayout(new TableLayout(new double[][]{
                {TableLayout.PREFERRED, TableLayout.PREFERRED},
                {TableLayout.PREFERRED, TableLayout.PREFERRED, TableLayout.PREFERRED, TableLayout.PREFERRED,
                        TableLayout.PREFERRED, TableLayout.PREFERRED, TableLayout.PREFERRED, TableLayout.PREFERRED}
        }));

        rightPanel.add(getPopulationLabel(), "0,0");
        WebSpinner populationSpinner = getPopulationSpinner();
        rightPanel.add(populationSpinner, "1,0");
        rightPanel.add(getCrossingoverLabel(), "0,1");
        rightPanel.add(getCrossingoverSpinner(), "1,1");
        rightPanel.add(getMutationLabel(), "0,2");
        rightPanel.add(getMutationSpinner(), "1,2");

        WebButton generateButton = getGenerateButton();
        WebButton nextStepButton = getNextStepButton();
        rightPanel.add(generateButton, "0,4,1,4");
        rightPanel.add(nextStepButton, "0,5,1,5");
        rightPanel.add(getFileChooserButton(generateButton, nextStepButton), "0,3,1,3");

        rightPanel.add(getAutoSwitcher(nextStepButton), "0,6,1,6");

        rightPanel.add(new WebLabel("Лучшее значение: "), "0,7");
        rightPanel.add(getBestYValueLabel(), "1,7");

        return rightPanel;
    }

    private WebLabel getBestYValueLabel() {
        WebLabel webLabel = new WebLabel("");
        context.setBestYLabel(webLabel);
        webLabel.setPadding(20, 15, 20, 0);
        return webLabel;
    }

    private WebPanel getAutoSwitcher(WebButton nextStepButton) {
        Clicker clicker = new Clicker(nextStepButton);
        clicker.start();

        WebSwitch webSwitch = new WebSwitch();
        webSwitch.setSwitchComponents("Авто", "Ручной");
        webSwitch.addActionListener(e -> clicker.setState(webSwitch.isSelected()));
        return new WebPanel(webSwitch);
    }

    private WebLabel getPopulationLabel() {
        return new WebLabel("Размер популяции: ");
    }

    private WebSpinner getPopulationSpinner() {
        SpinnerNumberModel numberModel = new SpinnerNumberModel(POPULATION_DEFAULT_VALUE, 4, MAX_POPULATION, 10);
        context.setPopulation(POPULATION_DEFAULT_VALUE);

        WebSpinner spinner = new WebSpinner();
        spinner.addChangeListener(e -> context.setPopulation(Integer.parseInt(spinner.getValue().toString())));
        spinner.setModel(numberModel);
        return spinner;
    }

    private WebLabel getCrossingoverLabel() {
        return new WebLabel("Вероятность кроссинговера: ");
    }

    private WebSpinner getCrossingoverSpinner() {
        SpinnerNumberModel numberModel = new SpinnerNumberModel(CROSSINGOVER_DEFAULT_VALUE, 0.4, 1, 0.1);
        context.setCrossingoverChance(CROSSINGOVER_DEFAULT_VALUE);

        WebSpinner spinner = new WebSpinner();
        spinner.addChangeListener(e -> context.setCrossingoverChance(Double.parseDouble(spinner.getValue().toString())));
        spinner.setModel(numberModel);
        return spinner;
    }

    private WebLabel getMutationLabel() {
        return new WebLabel("Вероятность мутации: ");
    }

    private WebSpinner getMutationSpinner() {
        SpinnerNumberModel numberModel = new SpinnerNumberModel(MUTATION_DEFAULT_VALUE, 0, 1, 0.05);
        context.setMutationChance(MUTATION_DEFAULT_VALUE);

        WebSpinner spinner = new WebSpinner();
        spinner.addChangeListener(e -> context.setMutationChance(Double.parseDouble(spinner.getValue().toString())));
        spinner.setModel(numberModel);
        return spinner;
    }

    private WebButton getFileChooserButton(WebButton generateButton, WebButton nextStepButton) {
        WebButton button = new WebButton("Выберите файл...");
        button.addActionListener(new FileChooserActionListener(button, context));
        button.addActionListener(e -> generateButton.setEnabled(true));
        button.addActionListener(e -> nextStepButton.setEnabled(true));
        button.setPadding(5);
        button.setMargin(5);
        return button;
    }

    private WebButton getGenerateButton() {
        WebButton button = new WebButton("Обновить");
        button.addActionListener(new Lab3GenerateButtonCallback(context));
        button.setEnabled(false);
        button.setPadding(5);
        button.setMargin(5);
        context.setGenerateButton(button);
        return button;
    }

    private WebButton getNextStepButton() {
        WebButton button = new WebButton("Следующие 10 шагов");
        button.addActionListener(new Lab3NextStepCallback(context));
        button.setEnabled(false);
        button.setPadding(5);
        button.setMargin(5);
        return button;
    }

    private WebPanel getRightPanel() {
        WebPanel panel = new WebPanel();
        panel.setMargin(20, 0, 20, 20);
        panel.setLayout(new TableLayout(new double[][]{
                {TableLayout.PREFERRED},
                {TableLayout.PREFERRED, TableLayout.FILL}
        }));

        panel.add(new WebLabel("Лучший тур"), "0,0");
        WebList citiesList = getCitiesList();
        context.setBestTourList(citiesList);
        panel.add(new WebScrollPane(citiesList).setMaximumWidth(50), "0,1");

        return panel;
    }

    private WebList getCitiesList() {
        WebList list = new WebList();
        list.setEditable(false);
        return list;
    }
}

package by.bsuir.ga.ui;

import by.bsuir.ga.model.Context;
import by.bsuir.ga.ui.callback.generate.Lab1GenerateButtonCallback;
import by.bsuir.ga.ui.callback.next.Lab1NextStepCallback;
import by.bsuir.ga.util.Clicker;
import com.alee.extended.button.WebSwitch;
import com.alee.extended.layout.TableLayout;
import com.alee.laf.button.WebButton;
import com.alee.laf.label.WebLabel;
import com.alee.laf.panel.WebPanel;
import com.alee.laf.spinner.WebSpinner;
import org.jfree.chart.ChartFactory;
import org.jfree.chart.ChartPanel;
import org.jfree.chart.JFreeChart;
import org.jfree.chart.plot.PlotOrientation;
import org.jfree.chart.renderer.xy.XYLineAndShapeRenderer;
import org.jfree.data.xy.DefaultXYDataset;

import javax.swing.*;
import java.awt.*;
import java.util.Arrays;
import java.util.stream.DoubleStream;

public class Lab1Tab extends WebPanel {

    private static final String MAIN_CHART_TITLE = "f(t) = (t + 1.3) * sin(0.5 * PI * t + 1), t [0,7]";
    private static final String DYNAMIC_CHART_TITLE = "Динамика лучшего и среднего значений";
    private static final String FUNCTION_SERIES_KEY = "Function";
    private static final Color BACKGROUND_COLOR = new Color(210, 210, 210);

    private static final int MAX_POPULATION = 80;
    private static final int POPULATION_DEFAULT_VALUE = 10;
    private static final int GENS_DEFAULT_VALUE = 10;
    private static final double CROSSINGOVER_DEFAULT_VALUE = 0.8;
    private static final double MUTATION_DEFAULT_VALUE = 0.1;

    private final Context<Long, Double> context;

    public Lab1Tab(Context<Long, Double> context) {
        this.context = context;

        this.setLayout(new TableLayout(new double[][]{
                {TableLayout.FILL, TableLayout.PREFERRED},
                {TableLayout.FILL}
        }));
        this.add(getLeftPanel(), "0,0");
        this.add(getRightPanel(), "1,0");
    }

    private WebPanel getLeftPanel() {
        WebPanel leftPanel = new WebPanel();
        leftPanel.setLayout(new TableLayout(new double[][]{
                {TableLayout.FILL},
                {TableLayout.FILL, TableLayout.FILL, TableLayout.FILL}
        }));

        leftPanel.add(getMainChartPanel(), "0,0,0,1");
        leftPanel.add(getDynamicChartPanel(), "0,2");

        return leftPanel;
    }

    private ChartPanel getMainChartPanel() {
        double increment = 0.05;
        double[] xValues = DoubleStream.iterate(context.getMinValue(), operand -> operand + increment)
                .limit((long) ((context.getMaxValue() - context.getMinValue()) / increment) + 1)
                .toArray();
        double[] yValues = Arrays.stream(xValues)
                .map(context.getFunction()::calculate)
                .toArray();
        DefaultXYDataset dataset = new DefaultXYDataset();
        dataset.addSeries(FUNCTION_SERIES_KEY, new double[][]{xValues, yValues});

        JFreeChart chart = ChartFactory.createXYLineChart(
                MAIN_CHART_TITLE, "X", "F(X)", dataset, PlotOrientation.VERTICAL, false, true, false
        );
        chart.getPlot().setBackgroundPaint(BACKGROUND_COLOR);

        JFreeChart tempScatterChart = ChartFactory.createScatterPlot("", "", "", dataset);
        chart.getXYPlot().setRenderer(1, tempScatterChart.getXYPlot().getRenderer());
        chart.getXYPlot().setDataset(1, new DefaultXYDataset());

        chart.getXYPlot().getDomainAxis().setRange(context.getMinValue() - 0.25, context.getMaxValue() + 0.25);
        context.setChart(chart);

        return new ChartPanel(chart);
    }

    private ChartPanel getDynamicChartPanel() {
        DefaultXYDataset dataset = new DefaultXYDataset();
        JFreeChart chart = ChartFactory.createXYLineChart(
                DYNAMIC_CHART_TITLE, "Шаг", "F(X)", dataset, PlotOrientation.VERTICAL, true, true, false
        );
        context.setExtraChart(chart);

        XYLineAndShapeRenderer renderer1 = new XYLineAndShapeRenderer(true, true);
        XYLineAndShapeRenderer renderer2 = new XYLineAndShapeRenderer(true, true);

        context.getExtraChart().getXYPlot().setRenderer(0, renderer1);
        context.getExtraChart().getXYPlot().setRenderer(1, renderer2);
        context.getExtraChart().getXYPlot().setDataset(0, new DefaultXYDataset());
        context.getExtraChart().getXYPlot().setDataset(1, new DefaultXYDataset());
        context.getExtraChart().getXYPlot().getDomainAxis().setRange(0, 5);

        return new ChartPanel(chart);
    }

    private WebPanel getRightPanel() {
        WebPanel rightPanel = new WebPanel();
        rightPanel.setMargin(20);
        rightPanel.setLayout(new TableLayout(new double[][]{
                {TableLayout.PREFERRED, TableLayout.PREFERRED},
                {TableLayout.PREFERRED, TableLayout.PREFERRED, TableLayout.PREFERRED, TableLayout.PREFERRED,
                        TableLayout.PREFERRED, TableLayout.PREFERRED, TableLayout.PREFERRED, TableLayout.PREFERRED}
        }));

        WebButton nextStepButton = getNextStepButton();

        rightPanel.add(getPopulationLabel(), "0,0");
        WebSpinner populationSpinner = getPopulationSpinner();
        rightPanel.add(populationSpinner, "1,0");
        rightPanel.add(getGensLabel(), "0,1");
        rightPanel.add(getGensSpinner(nextStepButton, ((SpinnerNumberModel) populationSpinner.getModel())), "1,1");
        rightPanel.add(getCrossingoverLabel(), "0,2");
        rightPanel.add(getCrossingoverSpinner(), "1,2");
        rightPanel.add(getMutationLabel(), "0,3");
        rightPanel.add(getMutationSpinner(), "1,3");

        rightPanel.add(getGenerateButton(nextStepButton), "0,4,1,4");
        rightPanel.add(nextStepButton, "0,5,1,5");
        rightPanel.add(getAutoSwitcher(nextStepButton), "0,6,1,6");
        rightPanel.add(getStatisticInfoLabel(), "0,7,1,7");

        return rightPanel;
    }

    private WebPanel getAutoSwitcher(WebButton nextStepButton) {
        Clicker clicker = new Clicker(nextStepButton);
        clicker.start();

        WebSwitch webSwitch = new WebSwitch();
        webSwitch.setSwitchComponents("Авто", "Ручной");
        webSwitch.addActionListener(e -> clicker.setState(webSwitch.isSelected()));
        return new WebPanel(webSwitch);
    }

    private WebLabel getPopulationLabel() {
        return new WebLabel("Размер популяции: ");
    }

    private WebSpinner getPopulationSpinner() {
        SpinnerNumberModel numberModel = new SpinnerNumberModel(POPULATION_DEFAULT_VALUE, 4, MAX_POPULATION, 2);
        context.setPopulation(POPULATION_DEFAULT_VALUE);

        WebSpinner spinner = new WebSpinner();
        spinner.addChangeListener(e -> context.setPopulation(Integer.parseInt(spinner.getValue().toString())));
        spinner.setModel(numberModel);
        return spinner;
    }

    private WebLabel getGensLabel() {
        return new WebLabel("Количество генов: ");
    }

    private WebSpinner getGensSpinner(WebButton nextStepButton, SpinnerNumberModel populationSpinnerModel) {
        SpinnerNumberModel numberModel = new SpinnerNumberModel(GENS_DEFAULT_VALUE, 4, 30, 1);
        context.setNumberOfGens(GENS_DEFAULT_VALUE);

        WebSpinner spinner = new WebSpinner();
        spinner.addChangeListener(e -> context.setNumberOfGens(Integer.parseInt(spinner.getValue().toString())));
        spinner.addChangeListener(e -> nextStepButton.setEnabled(false));
        spinner.addChangeListener(e -> {
            int spinnerValue = numberModel.getNumber().intValue();
            if (spinnerValue < 10) {
                int maxPopulation = 2 << spinnerValue - 1;
                int population = Math.min(populationSpinnerModel.getNumber().intValue(), maxPopulation);
                context.setPopulation(population);
                populationSpinnerModel.setValue(population);
                populationSpinnerModel.setMaximum(Math.min(MAX_POPULATION, maxPopulation));
            }
        });
        spinner.setModel(numberModel);
        return spinner;
    }

    private WebLabel getCrossingoverLabel() {
        return new WebLabel("Вероятность кроссинговера: ");
    }

    private WebSpinner getCrossingoverSpinner() {
        SpinnerNumberModel numberModel = new SpinnerNumberModel(CROSSINGOVER_DEFAULT_VALUE, 0.4, 1, 0.1);
        context.setCrossingoverChance(CROSSINGOVER_DEFAULT_VALUE);

        WebSpinner spinner = new WebSpinner();
        spinner.addChangeListener(e -> context.setCrossingoverChance(Double.parseDouble(spinner.getValue().toString())));
        spinner.setModel(numberModel);
        return spinner;
    }

    private WebLabel getMutationLabel() {
        return new WebLabel("Вероятность мутации: ");
    }

    private WebSpinner getMutationSpinner() {
        SpinnerNumberModel numberModel = new SpinnerNumberModel(MUTATION_DEFAULT_VALUE, 0, 0.6, 0.05);
        context.setMutationChance(MUTATION_DEFAULT_VALUE);

        WebSpinner spinner = new WebSpinner();
        spinner.addChangeListener(e -> context.setMutationChance(Double.parseDouble(spinner.getValue().toString())));
        spinner.setModel(numberModel);
        return spinner;
    }

    private WebButton getGenerateButton(WebButton nextStepButton) {
        WebButton button = new WebButton("Обновить");
        button.addActionListener(new Lab1GenerateButtonCallback(context));
        button.addActionListener(e -> nextStepButton.setEnabled(true));
        button.addActionListener(e -> {
            context.getChart().getXYPlot().getDomainAxis().setRange(context.getMinValue() - 0.25, context.getMaxValue() + 0.25);
//            context.getChart().getXYPlot().getRangeAxis().setRange(context.getMinValue() - 1, context.getMaxValue() + 1);
        });
        button.setPadding(5);
        button.setMargin(5);
        return button;
    }

    private WebButton getNextStepButton() {
        WebButton button = new WebButton("Следующий шаг");
        button.addActionListener(new Lab1NextStepCallback(context));
        button.setEnabled(false);
        button.setPadding(5);
        button.setMargin(5);
        return button;
    }

    private WebPanel getStatisticInfoLabel() {
        WebPanel panel = new WebPanel();
        panel.setMargin(20, 0, 0, 0);
        panel.setLayout(new TableLayout(new double[][]{
                {TableLayout.PREFERRED, TableLayout.PREFERRED},
                {TableLayout.FILL, TableLayout.FILL, TableLayout.FILL}
        }));

        panel.add(new WebLabel("Лучшая особь").changeFontSize(6), "0,0,1,0");
        panel.add(new WebLabel("Значение особи:"), "0,1");
        panel.add(getBestXValueLabel(), "1,1");
        panel.add(new WebLabel("Значение функции:"), "0,2");
        panel.add(getBestYValueLabel(), "1,2");

        return panel;
    }

    private WebLabel getBestXValueLabel() {
        WebLabel webLabel = new WebLabel("");
        webLabel.setPadding(0, 15, 0, 0);
        context.setBestXLabel(webLabel);
        return webLabel;
    }

    private WebLabel getBestYValueLabel() {
        WebLabel webLabel = new WebLabel("");
        context.setBestYLabel(webLabel);
        webLabel.setPadding(0, 15, 0, 0);
        return webLabel;
    }
}

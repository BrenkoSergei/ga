package by.bsuir.ga.ui.callback.next;

import by.bsuir.ga.model.Chromosome;
import by.bsuir.ga.model.City;
import by.bsuir.ga.model.Context;
import by.bsuir.ga.util.BiPair;
import com.alee.laf.button.WebButton;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Random;
import java.util.Set;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class Lab3NextStepCallback implements ActionListener {

    private static final int PRECISION_MULTIPLIER = 100_000;
    private static final int MAX_MUTATION_SWAPS = 1;

    private final Random random = new Random(System.currentTimeMillis());
    private final Context<Object, List<City>> context;

    public Lab3NextStepCallback(Context<Object, List<City>> context) {
        this.context = context;
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        ((WebButton) e.getSource()).setEnabled(false);
        try {
            for (int i = 0; i < 10; i++) {
                List<Chromosome<Object, List<City>>> chromosomes = context.getChromosomes();

                List<Chromosome<Object, List<City>>> intermediatePopulation = getIntermediatePopulation(chromosomes);
                List<BiPair<Chromosome<Object, List<City>>>> pairs = getIntermediatePopulationPairs(intermediatePopulation);

                List<Chromosome<Object, List<City>>> newGeneration = getNewGeneration(pairs);

                List<Chromosome<Object, List<City>>> newPopulation = getNewPopulation(chromosomes, newGeneration);

                context.setChromosomes(newPopulation);
                context.recalculate();
            }
        } finally {
            ((WebButton) e.getSource()).setEnabled(true);
        }
    }

    private List<Chromosome<Object, List<City>>> getIntermediatePopulation(List<Chromosome<Object, List<City>>> chromosomes) {
        double a = random.nextDouble() + 1;
        double b = 2 - a;
        AtomicInteger ppc = new AtomicInteger(0);
        List<Chromosome<Object, List<City>>> orderedChromosomes = chromosomes.stream()
                .sorted(Comparator.comparing(Chromosome::getActualY))
                .parallel()
                .peek(chromosome ->
                        chromosome.setNormalizedValue(calculateProbability(a, b, ppc.incrementAndGet(), chromosomes.size()))
                )
                .collect(Collectors.toList());

        List<Chromosome<Object, List<City>>> result = new ArrayList<>();

        while (result.size() < chromosomes.size()) {
            int randomValue = random.nextInt(PRECISION_MULTIPLIER);

            int randomCounter = 0;
            for (Chromosome<Object, List<City>> chromosome : orderedChromosomes) {
                randomCounter += chromosome.getNormalizedValue() * PRECISION_MULTIPLIER;

                if (randomCounter >= randomValue) {
                    result.add(chromosome);
                    break;
                }
            }
        }

        return result;
    }

    // i >= 1
    private double calculateProbability(double a, double b, int i, int n) {
        return 1.0 / n * (a - (a - b) * (i - 1.0) / (n - 1.0));
    }

    private List<BiPair<Chromosome<Object, List<City>>>> getIntermediatePopulationPairs(List<Chromosome<Object, List<City>>> intermediatePopulation) {
        List<Chromosome<Object, List<City>>> sortedResults = intermediatePopulation.stream()
                .sorted(Comparator.comparing(Chromosome::getActualY))
                .collect(Collectors.toList());

        List<BiPair<Chromosome<Object, List<City>>>> result = new ArrayList<>();
        Map<Chromosome<Object, List<City>>, Set<Chromosome>> chromosomeToPartners = new HashMap<>();
        while (!sortedResults.isEmpty()) {
            Chromosome<Object, List<City>> firstChromosome = sortedResults.get(0);
            if (!chromosomeToPartners.containsKey(firstChromosome)) {
                chromosomeToPartners.clear();
            }
            Set<Chromosome> partners = chromosomeToPartners.computeIfAbsent(firstChromosome, c -> new HashSet<>());
            Chromosome<Object, List<City>> secondChromosome = sortedResults.stream()
                    .filter(chromosome -> !chromosome.equals(firstChromosome) && !partners.contains(chromosome))
                    .findFirst()
                    .orElse(null);

            if (secondChromosome != null) {
                result.add(new BiPair<>(firstChromosome, secondChromosome));
                partners.add(secondChromosome);
                sortedResults.remove(0);
                sortedResults.remove(secondChromosome);
            } else {
                break;
            }
        }
        return result;
    }

    private List<Chromosome<Object, List<City>>> getNewGeneration(List<BiPair<Chromosome<Object, List<City>>>> pairs) {
        List<Chromosome<Object, List<City>>> newGeneration = pairs.stream()
                .parallel()
                .filter(pair -> random.nextDouble() < context.getCrossingoverChance())
                .flatMap(this::performCrossingover)
                .peek(chromosome -> chromosome.setActualY(context.getFunction().calculate(chromosome.getActualX())))
                .collect(Collectors.toList());

        newGeneration.stream()
                .parallel()
                .filter(chromosome -> random.nextDouble() < context.getMutationChance())
                .peek(this::performMutation)
                .forEach(chromosome -> chromosome.setActualY(context.getFunction().calculate(chromosome.getActualX())));
        return newGeneration;
    }

    private Stream<? extends Chromosome<Object, List<City>>> performCrossingover(BiPair<Chromosome<Object, List<City>>> chromosomeBiPair) {
        List<City> tour1 = chromosomeBiPair.getFirst().getActualX();
        List<City> tour2 = chromosomeBiPair.getSecond().getActualX();

        List<City> newTour1 = performCyclicCrossingover(tour1, tour2);
        List<City> newTour2 = performCyclicCrossingover(tour2, tour1);

        Chromosome<Object, List<City>> chromosome1 = new Chromosome<>(null, newTour1);
        Chromosome<Object, List<City>> chromosome2 = new Chromosome<>(null, newTour2);

        return Stream.of(chromosome1, chromosome2);
    }

    private List<City> performCyclicCrossingover(List<City> tour1, List<City> tour2) {
        List<City> newTour = Stream.generate(() -> ((City) null))
                .limit(tour1.size())
                .collect(Collectors.toList());
        int i = 0;
        while (newTour.get(i) == null) {
            newTour.set(i, tour1.get(i));
            i = tour1.indexOf(tour2.get(i));
        }
        for (i = 0; i < newTour.size(); i++) {
            if (newTour.get(i) == null) {
                newTour.set(i, tour2.get(i));
            }
        }
        return newTour;
    }

    private void performMutation(Chromosome<Object, List<City>> chromosome) {
        int[] indexes = random.ints(1, chromosome.getActualX().size())
                .distinct()
                .limit(2)
                .sorted()
                .toArray();
        List<City> cities = chromosome.getActualX().subList(indexes[0], indexes[1]);
        if (random.nextDouble() < 0.9) {
            Collections.reverse(cities);
        } else {
            Collections.shuffle(cities);
        }
    }

    private List<Chromosome<Object, List<City>>> getNewPopulation(List<Chromosome<Object, List<City>>> chromosomes,
                                                                  List<Chromosome<Object, List<City>>> newGeneration) {
        return Stream.concat(chromosomes.stream(), newGeneration.stream())
                .distinct()
                .sorted(Comparator.comparing(Chromosome::getActualY))
                .limit(context.getPopulation())
                .collect(Collectors.toList());
    }
}

package by.bsuir.ga.ui.callback.generate;

import by.bsuir.ga.model.Chromosome;
import by.bsuir.ga.model.Context;
import by.bsuir.ga.util.CalculationUtils;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.List;
import java.util.Random;
import java.util.stream.Collectors;
import java.util.stream.Stream;

abstract class GenerateButtonCallback<RX, AX> implements ActionListener {

    final Random random = new Random(System.currentTimeMillis());
    final Context<RX, AX> context;

    GenerateButtonCallback(Context<RX, AX> context) {
        this.context = context;
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        long countOfX = (long) Math.pow(2, context.getNumberOfGens());
        List<Chromosome<RX, AX>> chromosomes = generateChromosomes(countOfX)
                .peek(chromosome -> chromosome.setActualY(context.getFunction().calculate(chromosome.getActualX())))
                .collect(Collectors.toList());
        context.setChromosomes(chromosomes);

        context.getBestValues().clear();
        context.getAverageValues().clear();
        context.recalculate();
    }

    abstract Stream<Chromosome<RX, AX>> generateChromosomes(long boundOfX);

    double calculateActualX(long operand) {
        return CalculationUtils.toReal(operand, context.getMinValue(), context.getMaxValue(), context.getNumberOfGens());
    }
}

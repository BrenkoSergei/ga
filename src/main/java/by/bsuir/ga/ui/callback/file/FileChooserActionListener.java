package by.bsuir.ga.ui.callback.file;

import by.bsuir.ga.model.Context;
import by.bsuir.ga.util.TspParser;
import com.alee.laf.button.WebButton;
import com.alee.laf.filechooser.WebFileChooser;
import com.alee.utils.FileUtils;

import javax.swing.filechooser.FileNameExtensionFilter;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;

public class FileChooserActionListener implements ActionListener {

    private final WebFileChooser fileChooser = new WebFileChooser("E://ga/test problems/");
    private final TspParser tspParser = new TspParser();
    private final Context<?, ?> context;
    private final WebButton owner;
    private File file = null;

    public FileChooserActionListener(WebButton owner, Context<?, ?> context) {
        this.context = context;
        this.owner = owner;
        this.fileChooser.setMultiSelectionEnabled(false);

        FileNameExtensionFilter filter = new FileNameExtensionFilter("TSP File", "tsp");
        this.fileChooser.setFileFilter(filter);
        this.fileChooser.setAcceptAllFileFilterUsed(false);
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        if (file != null) {
            fileChooser.setSelectedFile(file);
        }
        if (fileChooser.showOpenDialog(owner) == WebFileChooser.APPROVE_OPTION) {
            file = fileChooser.getSelectedFile();
            context.setTsp(tspParser.parse(file));
            owner.setIcon(FileUtils.getFileIcon(file));
            owner.setText(FileUtils.getDisplayFileName(file));
            context.clickGenerateButton();
        }
    }
}

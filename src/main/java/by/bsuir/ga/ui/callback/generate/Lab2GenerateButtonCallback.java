package by.bsuir.ga.ui.callback.generate;

import by.bsuir.ga.model.Chromosome;
import by.bsuir.ga.model.Context;
import by.bsuir.ga.util.BiPair;

import java.util.stream.Stream;

public class Lab2GenerateButtonCallback extends GenerateButtonCallback<BiPair<Long>, BiPair<Double>> {

    public Lab2GenerateButtonCallback(Context<BiPair<Long>, BiPair<Double>> context) {
        super(context);
    }

    @Override
    Stream<Chromosome<BiPair<Long>, BiPair<Double>>> generateChromosomes(long boundOfX) {
        return Stream.generate(() -> new BiPair<>(nextLong(boundOfX), nextLong(boundOfX)))
                .distinct()
                .limit(context.getPopulation())
                .map(relatedXes -> new Chromosome<>(relatedXes, toActualXes(relatedXes)));
    }

    private long nextLong(long bound) {
        return random.longs(1, 0, bound)
                .findFirst()
                .orElse(0);
    }

    private BiPair<Double> toActualXes(BiPair<Long> relatedXes) {
        return new BiPair<>(calculateActualX(relatedXes.getFirst()), calculateActualX(relatedXes.getSecond()));
    }
}

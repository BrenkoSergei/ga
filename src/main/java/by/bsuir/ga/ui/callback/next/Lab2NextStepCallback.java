package by.bsuir.ga.ui.callback.next;

import by.bsuir.ga.model.Chromosome;
import by.bsuir.ga.model.Context;
import by.bsuir.ga.util.BiPair;
import by.bsuir.ga.util.CalculationUtils;
import by.bsuir.ga.util.NumericUtils;
import com.alee.laf.button.WebButton;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.IdentityHashMap;
import java.util.List;
import java.util.Map;
import java.util.Random;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class Lab2NextStepCallback implements ActionListener {

    private static final int PRECISION_MULTIPLIER = 100_000;

    private final Random random = new Random(System.currentTimeMillis());
    private final Context<BiPair<Long>, BiPair<Double>> context;

    public Lab2NextStepCallback(Context<BiPair<Long>, BiPair<Double>> context) {
        this.context = context;
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        ((WebButton) e.getSource()).setEnabled(false);

        List<Chromosome<BiPair<Long>, BiPair<Double>>> chromosomes = context.getChromosomes();

        List<Chromosome<BiPair<Long>, BiPair<Double>>> intermediatePopulation = getIntermediatePopulation(chromosomes);
        List<BiPair<Chromosome<BiPair<Long>, BiPair<Double>>>> pairs = getIntermediatePopulationPairs(intermediatePopulation);

        List<Chromosome<BiPair<Long>, BiPair<Double>>> newGeneration = getNewGeneration(pairs);

        List<Chromosome<BiPair<Long>, BiPair<Double>>> newPopulation = getNewPopulation(chromosomes, newGeneration);

        context.setChromosomes(newPopulation);
        context.recalculate();

        ((WebButton) e.getSource()).setEnabled(true);
    }

    private List<Chromosome<BiPair<Long>, BiPair<Double>>> getIntermediatePopulation(List<Chromosome<BiPair<Long>, BiPair<Double>>> chromosomes) {
        double a = random.nextDouble() + 1;
        double b = 2 - a;
        AtomicInteger ppc = new AtomicInteger(0);
        List<Chromosome<BiPair<Long>, BiPair<Double>>> orderedChromosomes = chromosomes.stream()
                .sorted(Comparator.comparing(Chromosome::getActualY))
                .peek(chromosome ->
                        chromosome.setNormalizedValue(calculateProbability(a, b, ppc.incrementAndGet(), context.getPopulation()))
                )
                .collect(Collectors.toList());

        List<Chromosome<BiPair<Long>, BiPair<Double>>> result = new ArrayList<>();
        Map<Chromosome<BiPair<Long>, BiPair<Double>>, Integer> summaryToEntries = new IdentityHashMap<>();

        while (result.size() < chromosomes.size()) {
            int randomValue = random.nextInt(PRECISION_MULTIPLIER);

            int randomCounter = 0;
            for (Chromosome<BiPair<Long>, BiPair<Double>> chromosome : orderedChromosomes) {
                Integer entries = summaryToEntries.getOrDefault(chromosome, 0);
                randomCounter += chromosome.getNormalizedValue() * PRECISION_MULTIPLIER;

                if (randomCounter >= randomValue && entries < orderedChromosomes.size() / 3) {
                    result.add(chromosome);
                    summaryToEntries.put(chromosome, ++entries);
                    break;
                }
            }
        }

        return result;
    }

    private String joinXes(List<Chromosome<BiPair<Long>, BiPair<Double>>> chromosomes) {
        return chromosomes.stream()
                .map(Chromosome::getActualX)
                .map(this::getString)
                .collect(Collectors.joining(", "));
    }

    private String getString(BiPair<Double> pair) {
        return "[" + NumericUtils.toString(pair.getFirst()) + " - " + NumericUtils.toString(pair.getSecond()) + "]";
    }

    private String joinXes2(List<BiPair<Chromosome<BiPair<Long>, BiPair<Double>>>> chromosomes) {
        return chromosomes.stream()
                .map(pair -> getString(pair.getFirst().getActualX()) + " - " + getString(pair.getSecond().getActualX()))
                .collect(Collectors.joining(", "));
    }

    // i >= 1
    private double calculateProbability(double a, double b, int i, int n) {
        return 1.0 / n * (a - (a - b) * (i - 1.0) / (n - 1.0));
    }

    private List<BiPair<Chromosome<BiPair<Long>, BiPair<Double>>>> getIntermediatePopulationPairs(List<Chromosome<BiPair<Long>, BiPair<Double>>> intermediatePopulation) {
        Collections.shuffle(intermediatePopulation);

        List<BiPair<Chromosome<BiPair<Long>, BiPair<Double>>>> result = new ArrayList<>();
        for (int i = 0; i < intermediatePopulation.size() / 2 * 2; i += 2) {
            result.add(new BiPair<>(intermediatePopulation.get(i), intermediatePopulation.get(i + 1)));
        }

        return result;
    }

    private List<Chromosome<BiPair<Long>, BiPair<Double>>> getNewGeneration(List<BiPair<Chromosome<BiPair<Long>, BiPair<Double>>>> pairs) {
        long mask = random.longs(1, 0, 2L << context.getNumberOfGens() - 1)
                .findFirst()
                .orElse(0);
        long invertedMask = (2 << context.getNumberOfGens() - 1) - 1 - mask;

        List<Chromosome<BiPair<Long>, BiPair<Double>>> collect = pairs.stream()
                .filter(chromosome -> random.nextDouble() < context.getCrossingoverChance())
                .flatMap(chromosomeBiPair -> performCrossingover(mask, invertedMask, chromosomeBiPair))
                .peek(chromosome -> chromosome.setActualY(context.getFunction().calculate(chromosome.getActualX())))
                .collect(Collectors.toList());

        collect.stream()
                .filter(chromosome -> random.nextDouble() < context.getMutationChance())
                .peek(this::performMutation)
                .forEach(chromosome -> chromosome.setActualY(context.getFunction().calculate(chromosome.getActualX())));

        return collect;
    }

    private Stream<? extends Chromosome<BiPair<Long>, BiPair<Double>>> performCrossingover(long mask, long invertedMask, BiPair<Chromosome<BiPair<Long>, BiPair<Double>>> chromosomeBiPair) {
        BiPair<Long> relativeX1 = chromosomeBiPair.getFirst().getRelativeX();
        BiPair<Long> relativeX2 = chromosomeBiPair.getSecond().getRelativeX();

        BiPair<Long> newRelativeX1 = new BiPair<>(
                (relativeX1.getFirst() & mask) + (relativeX2.getFirst() & invertedMask),
                (relativeX1.getSecond() & mask) + (relativeX2.getSecond() & invertedMask)
        );
        BiPair<Long> newRelativeX2 = new BiPair<>(
                (relativeX1.getFirst() & invertedMask) + (relativeX2.getFirst() & mask),
                (relativeX1.getSecond() & invertedMask) + (relativeX2.getSecond() & mask)
        );

        Chromosome<BiPair<Long>, BiPair<Double>> chromosome1 = new Chromosome<>(newRelativeX1, toActualXes(newRelativeX1));
        Chromosome<BiPair<Long>, BiPair<Double>> chromosome2 = new Chromosome<>(newRelativeX2, toActualXes(newRelativeX2));

        return Stream.of(chromosome1, chromosome2);
    }

    private BiPair<Double> toActualXes(BiPair<Long> relatedXes) {
        return new BiPair<>(toActualX(relatedXes.getFirst()), toActualX(relatedXes.getSecond()));
    }

    private double toActualX(long operand) {
        return CalculationUtils.toReal(operand, context.getMinValue(), context.getMaxValue(), context.getNumberOfGens());
    }

    private void performMutation(Chromosome<BiPair<Long>, BiPair<Double>> chromosome) {
        Integer numberOfGens = context.getNumberOfGens();
        if (random.nextDouble() < context.getMutationChance()) {
            long x = chromosome.getRelativeX().getFirst();
            long reversedNumber = randomlyReverseBits(x, numberOfGens);
            chromosome.setRelativeX(new BiPair<>(reversedNumber, chromosome.getRelativeX().getSecond()));
            chromosome.setActualX(new BiPair<>(toActualX(reversedNumber), toActualX(chromosome.getRelativeX().getSecond())));
        }
        if (random.nextDouble() < context.getMutationChance()) {
            long x = chromosome.getRelativeX().getSecond();
            long reversedNumber = randomlyReverseBits(x, numberOfGens);
            chromosome.setRelativeX(new BiPair<>(chromosome.getRelativeX().getFirst(), reversedNumber));
            chromosome.setActualX(new BiPair<>(toActualX(chromosome.getRelativeX().getFirst()), toActualX(reversedNumber)));
        }
    }

    private long randomlyReverseBits(long value, Integer numberOfGens) {
        int[] bitPositions = random.ints(0, numberOfGens)
                .distinct()
                .limit(2)
                .sorted()
                .toArray();

        StringBuilder number = new StringBuilder(Long.toBinaryString(value)).reverse();
        for (int i = 0; i < numberOfGens - number.length(); i++) {
            number.append('0');
        }

        int maxIndex = Math.min(bitPositions[1] + 1, number.length());
        String reversed = new StringBuilder(number.substring(maxIndex)).reverse()
                .append(number.substring(bitPositions[0], maxIndex))
                .append(new StringBuilder(number.substring(0, bitPositions[0])).reverse())
                .toString();
        return Long.parseLong(reversed, 2);
    }

    private List<Chromosome<BiPair<Long>, BiPair<Double>>> getNewPopulation(List<Chromosome<BiPair<Long>, BiPair<Double>>> chromosomes,
                                                                            List<Chromosome<BiPair<Long>, BiPair<Double>>> newGeneration) {
        return Stream.concat(chromosomes.stream(), newGeneration.stream())
                .distinct()
                .sorted(Comparator.comparing(Chromosome::getActualY))
                .limit(context.getPopulation())
                .collect(Collectors.toList());
    }
}

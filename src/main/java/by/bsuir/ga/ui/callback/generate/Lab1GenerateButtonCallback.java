package by.bsuir.ga.ui.callback.generate;

import by.bsuir.ga.model.Chromosome;
import by.bsuir.ga.model.Context;

import java.util.stream.Stream;

public class Lab1GenerateButtonCallback extends GenerateButtonCallback<Long, Double> {

    public Lab1GenerateButtonCallback(Context<Long, Double> context) {
        super(context);
    }

    @Override
    Stream<Chromosome<Long, Double>> generateChromosomes(long boundOfX) {
        return random.longs(0, boundOfX)
                .distinct()
                .limit(context.getPopulation())
                .mapToObj(operand -> new Chromosome<>(operand, calculateActualX(operand)));
    }
}

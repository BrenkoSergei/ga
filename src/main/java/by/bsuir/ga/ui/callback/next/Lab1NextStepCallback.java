package by.bsuir.ga.ui.callback.next;

import by.bsuir.ga.model.Chromosome;
import by.bsuir.ga.model.Context;
import by.bsuir.ga.util.CalculationUtils;
import by.bsuir.ga.util.Pair;
import com.alee.laf.button.WebButton;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.IdentityHashMap;
import java.util.List;
import java.util.Map;
import java.util.Random;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class Lab1NextStepCallback implements ActionListener {

    private static final double ADJUSTMENT_MULTIPLIER = 1.1;
    private static final int PRECISION_MULTIPLIER = 100_000;

    private final Random random = new Random(System.currentTimeMillis());
    private final Context<Long, Double> context;

    public Lab1NextStepCallback(Context<Long, Double> context) {
        this.context = context;
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        ((WebButton) e.getSource()).setEnabled(false);

        List<Chromosome<Long, Double>> chromosomes = context.getChromosomes();

        double sumOfAdjustedYs = adjustYs(chromosomes);
        updateNormalizedValues(chromosomes, sumOfAdjustedYs);

        List<Chromosome<Long, Double>> intermediatePopulation = getIntermediatePopulation(chromosomes);
        List<Pair<Chromosome<Long, Double>, Chromosome<Long, Double>>> pairs = getIntermediatePopulationPairs(intermediatePopulation);

        List<Chromosome<Long, Double>> newGeneration = getNewGeneration(pairs);

        List<Chromosome<Long, Double>> newPopulation = getNewPopulation(chromosomes, newGeneration);
        context.setChromosomes(newPopulation);
        context.recalculate();

        ((WebButton) e.getSource()).setEnabled(true);
    }

    private List<Chromosome<Long, Double>> getNewPopulation(List<Chromosome<Long, Double>> chromosomes, List<Chromosome<Long, Double>> newGeneration) {
        return Stream.concat(chromosomes.stream(), newGeneration.stream())
                .distinct()
                .sorted(Comparator.comparing(Chromosome<Long, Double>::getActualY).reversed())
                .limit(context.getPopulation())
                .collect(Collectors.toList());
    }

    private List<Chromosome<Long, Double>> getNewGeneration(List<Pair<Chromosome<Long, Double>, Chromosome<Long, Double>>> pairs) {
        List<Chromosome<Long, Double>> newGeneration = pairs.stream()
                .filter(pair -> random.nextDouble() < context.getCrossingoverChance())
                .flatMap(this::performCrossingover)
                .collect(Collectors.toList());
        newGeneration.stream()
                .filter(chromosome -> random.nextDouble() < context.getMutationChance())
                .forEach(this::performMutation);
        return newGeneration;
    }

    private void performMutation(Chromosome<Long, Double> chromosome) {
        Integer numberOfGens = context.getNumberOfGens();

        int randomBit = random.nextInt(numberOfGens);
        long mutatedX = chromosome.getRelativeX() ^ (1 << randomBit);

        chromosome.setRelativeX(mutatedX);
        chromosome.setActualX(CalculationUtils.toReal(mutatedX, context.getMinValue(), context.getMaxValue(), numberOfGens));
        chromosome.setActualY(context.getFunction().calculate(chromosome.getActualX()));
    }

    private Stream<? extends Chromosome<Long, Double>> performCrossingover(Pair<Chromosome<Long, Double>, Chromosome<Long, Double>> pair) {
        Chromosome<Long, Double> first = pair.getFirst();
        Chromosome<Long, Double> second = pair.getSecond();
        Integer numberOfGens = context.getNumberOfGens();
        int crossPoint = random.nextInt(numberOfGens - 1);

        long newX1 = CalculationUtils.getBeforeCrossPoint(first.getRelativeX(), numberOfGens, crossPoint)
                + CalculationUtils.getAfterCrossPoint(second.getRelativeX(), numberOfGens, crossPoint);
        long newX2 = CalculationUtils.getBeforeCrossPoint(second.getRelativeX(), numberOfGens, crossPoint)
                + CalculationUtils.getAfterCrossPoint(first.getRelativeX(), numberOfGens, crossPoint);

        int minValue = context.getMinValue();
        int maxValue = context.getMaxValue();
        Chromosome<Long, Double> chromosome1 = new Chromosome<>(newX1, CalculationUtils.toReal(newX1, minValue, maxValue, numberOfGens));
        Chromosome<Long, Double> chromosome2 = new Chromosome<>(newX2, CalculationUtils.toReal(newX2, minValue, maxValue, numberOfGens));

        chromosome1.setActualY(context.getFunction().calculate(chromosome1.getActualX()));
        chromosome2.setActualY(context.getFunction().calculate(chromosome2.getActualX()));

        return Stream.of(chromosome1, chromosome2);
    }

    private List<Pair<Chromosome<Long, Double>, Chromosome<Long, Double>>> getIntermediatePopulationPairs(List<Chromosome<Long, Double>> intermediatePopulation) {
        return intermediatePopulation.stream()
                .flatMap(summary -> intermediatePopulation.stream()
                        .filter(chromosomeSummary -> !summary.equals(chromosomeSummary))
                        .map(chromosomeSummary -> new Pair<>(summary, chromosomeSummary)))
                .limit(intermediatePopulation.size() / 2)
                .distinct()
                .collect(Collectors.toList());
    }

    private void updateNormalizedValues(List<Chromosome<Long, Double>> chromosomes, double sumOfAdjustedYs) {
        chromosomes.forEach(chromosomeSummary -> chromosomeSummary.updateNormalizedValue(sumOfAdjustedYs));
    }

    private double adjustYs(List<Chromosome<Long, Double>> chromosomes) {
        chromosomes.stream()
                .min(Comparator.comparing(Chromosome::getActualY))
                .map(Chromosome::getActualY)
                .filter(minValue -> minValue < 0)
                .ifPresent(minValue -> chromosomes.forEach(chromosome -> chromosome.adjustY(minValue * ADJUSTMENT_MULTIPLIER)));
        return chromosomes.stream().mapToDouble(Chromosome::getAdjustedY).sum();
    }

    private List<Chromosome<Long, Double>> getIntermediatePopulation(List<Chromosome<Long, Double>> chromosomes) {
        List<Chromosome<Long, Double>> result = new ArrayList<>();
        Map<Chromosome<Long, Double>, Integer> summaryToEntries = new IdentityHashMap<>();

        while (result.size() < chromosomes.size()) {
            int randomValue = random.nextInt(PRECISION_MULTIPLIER);

            int randomCounter = 0;
            for (Chromosome<Long, Double> summary : chromosomes) {
                Integer entries = summaryToEntries.getOrDefault(summary, 0);
                randomCounter += summary.getNormalizedValue() * PRECISION_MULTIPLIER;

                if (randomCounter >= randomValue && entries < chromosomes.size() / 3) {
                    result.add(summary);
                    summaryToEntries.put(summary, ++entries);
                    break;
                }
            }
        }

        return result;
    }
}

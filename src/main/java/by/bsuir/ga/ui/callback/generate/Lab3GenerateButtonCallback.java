package by.bsuir.ga.ui.callback.generate;

import by.bsuir.ga.model.Chromosome;
import by.bsuir.ga.model.City;
import by.bsuir.ga.model.Context;
import org.jfree.chart.JFreeChart;
import org.jfree.data.xy.DefaultXYDataset;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class Lab3GenerateButtonCallback implements ActionListener {

    private final Context<Object, List<City>> context;

    public Lab3GenerateButtonCallback(Context<Object, List<City>> context) {
        this.context = context;
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        JFreeChart chart = context.getChart();
        List<City> cities = context.getTsp().getCities();

        double[] xes = cities.stream()
                .mapToDouble(City::getX)
                .toArray();
        double[] yes = cities.stream()
                .mapToDouble(City::getY)
                .toArray();

        DefaultXYDataset dataset = new DefaultXYDataset();
        dataset.addSeries("Cities", new double[][]{xes, yes});

        chart.getXYPlot().setDataset(0, dataset);

        List<Chromosome<Object, List<City>>> chromosomes = Stream.generate(() -> new ArrayList<>(context.getTsp().getCities()))
                .limit(context.getPopulation())
                .peek(cities1 -> cities1.remove(0))
                .peek(Collections::shuffle)
                .peek(cities1 -> cities1.add(0, context.getTsp().getCities().get(0)))
                .map(biPairs -> new Chromosome<>(null, (List<City>) biPairs))
                .collect(Collectors.toList());

        context.getBestValues().clear();
        context.getAverageValues().clear();
        context.setChromosomes(chromosomes);
        context.recalculate();
    }
}

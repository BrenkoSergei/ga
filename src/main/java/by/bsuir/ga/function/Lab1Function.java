package by.bsuir.ga.function;

public class Lab1Function implements Function<Double> {

    @Override
    public double calculate(Double x) {
        return (x + 1.3) * Math.sin(0.5 * Math.PI * x + 1);
    }
}

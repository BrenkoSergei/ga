package by.bsuir.ga.function;

import by.bsuir.ga.util.BiPair;

public class Lab2Function implements Function<BiPair<Double>> {

    @Override
    public double calculate(BiPair<Double> x) {
        double x1 = x.getFirst();
        double x2 = x.getSecond();
        return Math.pow(x1 * x1 + x2 * x2, 0.25) * (Math.pow(Math.sin(50 * Math.pow(x1 * x1 + x2 * x2, 0.1)), 2) + 1);
    }
}

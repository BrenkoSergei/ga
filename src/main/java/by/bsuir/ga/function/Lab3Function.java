package by.bsuir.ga.function;

import by.bsuir.ga.model.City;

import java.util.List;

public class Lab3Function implements Function<List<City>> {

    @Override
    public double calculate(List<City> x) {
        double result = calculateDistance(x.get(0), x.get(x.size() - 1));
        for (int i = 0; i < x.size() - 1; i++) {
            result += calculateDistance(x.get(i), x.get(i + 1));
        }
        return result;
    }

    private double calculateDistance(City city1, City city2) {
        return Math.sqrt(
                Math.pow(city1.getX() - city2.getX(), 2)
                        + Math.pow(city1.getY() - city2.getY(), 2)
        );
    }
}

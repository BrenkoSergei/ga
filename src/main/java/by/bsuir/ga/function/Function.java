package by.bsuir.ga.function;

public interface Function<AX> {

    double calculate(AX x);
}

package by.bsuir.ga;

import by.bsuir.ga.calculator.DefaultCalculator;
import by.bsuir.ga.calculator.Lab2Calculator;
import by.bsuir.ga.calculator.Lab3Calculator;
import by.bsuir.ga.function.Lab1Function;
import by.bsuir.ga.function.Lab2Function;
import by.bsuir.ga.function.Lab3Function;
import by.bsuir.ga.model.City;
import by.bsuir.ga.model.Context;
import by.bsuir.ga.painter.Lab1Painter;
import by.bsuir.ga.painter.Lab2Painter;
import by.bsuir.ga.painter.Lab3Painter;
import by.bsuir.ga.ui.Lab1Tab;
import by.bsuir.ga.ui.Lab2Tab;
import by.bsuir.ga.ui.Lab3Tab;
import by.bsuir.ga.util.BiPair;
import com.alee.laf.WebLookAndFeel;
import com.alee.laf.rootpane.WebFrame;
import com.alee.laf.tabbedpane.WebTabbedPane;

import javax.swing.*;
import java.awt.*;
import java.util.List;

public class Runner {

    private static final int LAB_1_MIN_VALUE = 0;
    private static final int LAB_1_MAX_VALUE = 7;

    private static final int LAB_2_MIN_VALUE = -100;
    private static final int LAB_2_MAX_VALUE = 100;

    public static void main(String[] args) {
        SwingUtilities.invokeLater(() -> {
            WebLookAndFeel.install();

            WebFrame frame = new WebFrame("GA");
            frame.setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
            frame.setVisible(true);
            frame.setMinimumSize(new Dimension(1400, 1000));

            WebTabbedPane tabbedPane = new WebTabbedPane(WebTabbedPane.TOP);
            frame.add(tabbedPane);

            Context<Long, Double> tab1Context = new Context<>(new Lab1Function(), LAB_1_MIN_VALUE, LAB_1_MAX_VALUE, new DefaultCalculator<>(), new Lab1Painter());
            tabbedPane.addTab("Лаба 1", new Lab1Tab(tab1Context));

            Context<BiPair<Long>, BiPair<Double>> tab2Context = new Context<>(new Lab2Function(), LAB_2_MIN_VALUE, LAB_2_MAX_VALUE, new Lab2Calculator(), new Lab2Painter());
            tabbedPane.addTab("Лаба 2", new Lab2Tab(tab2Context));

            Context<Object, List<City>> tab3Context = new Context<>(new Lab3Function(), 0, 0, new Lab3Calculator(), new Lab3Painter());
            tabbedPane.addTab("Лаба 3", new Lab3Tab(tab3Context));
        });
    }
}

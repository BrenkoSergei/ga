package by.bsuir.ga.model;

import java.util.Objects;

public class Chromosome<RX, AX> {

    private RX relativeX;
    private AX actualX;
    private double actualY;
    private double adjustedY;
    private double normalizedValue;

    public Chromosome(RX relativeX, AX actualX) {
        this.relativeX = relativeX;
        this.actualX = actualX;
    }

    public RX getRelativeX() {
        return this.relativeX;
    }

    public void setRelativeX(RX relativeX) {
        this.relativeX = relativeX;
    }

    public AX getActualX() {
        return this.actualX;
    }

    public void setActualX(AX actualX) {
        this.actualX = actualX;
    }

    public double getActualY() {
        return this.actualY;
    }

    public void setActualY(double actualY) {
        this.actualY = actualY;
    }

    public double getAdjustedY() {
        return this.adjustedY;
    }

    public void adjustY(double delta) {
        this.adjustedY = this.actualY - delta;
    }

    public double getNormalizedValue() {
        return this.normalizedValue;
    }

    public void updateNormalizedValue(double sum) {
        this.normalizedValue = Math.abs(this.adjustedY / sum);
    }

    public void setNormalizedValue(double normalizedValue) {
        this.normalizedValue = normalizedValue;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Chromosome<?, ?> that = (Chromosome<?, ?>) o;
        return Objects.equals(actualX, that.actualX);
    }

    @Override
    public int hashCode() {
        return Objects.hash(actualX);
    }

    @Override
    public String toString() {
        return Double.toString(actualY);//"["/* + NumericUtils.toString(actualX) + ", " + NumericUtils.toString(actualY) + " (" + NumericUtils.toString(adjustedY)*/ + ")]";
    }
}

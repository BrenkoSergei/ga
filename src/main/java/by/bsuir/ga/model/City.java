package by.bsuir.ga.model;

import java.util.Objects;

public class City {

    private final String name;
    private final Double x;
    private final Double y;

    public City(String name, Double x, Double y) {
        this.name = name;
        this.x = x;
        this.y = y;
    }

    public String getName() {
        return name;
    }

    public Double getX() {
        return x;
    }

    public Double getY() {
        return y;
    }

    @Override
    public String toString() {
        return name + " [" + x + " - " + y + "]";
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        City city = (City) o;
        return Objects.equals(name, city.name) &&
                Objects.equals(x, city.x) &&
                Objects.equals(y, city.y);
    }

    @Override
    public int hashCode() {
        return Objects.hash(name, x, y);
    }
}

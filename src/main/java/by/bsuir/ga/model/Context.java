package by.bsuir.ga.model;

import by.bsuir.ga.calculator.Calculator;
import by.bsuir.ga.function.Function;
import by.bsuir.ga.painter.Painter;
import by.bsuir.ga.util.NumericUtils;
import com.alee.laf.button.WebButton;
import com.alee.laf.label.WebLabel;
import com.alee.laf.list.WebList;
import org.jfree.chart.JFreeChart;

import java.util.ArrayList;
import java.util.List;

public class Context<RX, AX> {

    private final Function<AX> function;
    private final int minValue;
    private final int maxValue;

    private final Calculator<RX, AX> calculator;
    private final Painter<RX, AX> painter;

    private Integer population;
    private Integer numberOfGens;
    private Double crossingoverChance;
    private Double mutationChance;

    private List<Chromosome<RX, AX>> chromosomes;
    private JFreeChart chart;
    private JFreeChart extraChart;

    private WebButton generateButton;
    private WebLabel bestXLabel;
    private WebLabel bestYLabel;
    private WebList bestTourList;

    private Chromosome<RX, AX> currentBestValue;
    private List<Double> bestValues = new ArrayList<>();
    private final List<Double> averageValues = new ArrayList<>();

    private Tsp tsp;

    public Context(Function<AX> function, int minValue, int maxValue, Calculator<RX, AX> calculator, Painter<RX, AX> painter) {
        this.function = function;
        this.minValue = minValue;
        this.maxValue = maxValue;

        this.calculator = calculator;
        this.painter = painter;
    }

    public void recalculate() {
        this.calculator.calculate(this);
        this.painter.paint(this);
    }

    public Function<AX> getFunction() {
        return function;
    }

    public int getMinValue() {
        return minValue;
    }

    public int getMaxValue() {
        return maxValue;
    }

    public Integer getPopulation() {
        return population;
    }

    public void setPopulation(Integer population) {
        this.population = population;
    }

    public Integer getNumberOfGens() {
        return numberOfGens;
    }

    public void setNumberOfGens(Integer numberOfGens) {
        this.numberOfGens = numberOfGens;
    }

    public Double getCrossingoverChance() {
        return crossingoverChance;
    }

    public void setCrossingoverChance(Double crossingoverChance) {
        this.crossingoverChance = crossingoverChance;
    }

    public Double getMutationChance() {
        return mutationChance;
    }

    public void setMutationChance(Double mutationChance) {
        this.mutationChance = mutationChance;
    }

    public List<Chromosome<RX, AX>> getChromosomes() {
        return chromosomes;
    }

    public void setChromosomes(List<Chromosome<RX, AX>> chromosomes) {
        this.chromosomes = chromosomes;
    }

    public JFreeChart getChart() {
        return chart;
    }

    public void setChart(JFreeChart chart) {
        this.chart = chart;
    }

    public JFreeChart getExtraChart() {
        return extraChart;
    }

    public void setExtraChart(JFreeChart extraChart) {
        this.extraChart = extraChart;
    }

    public void addBestChromosome(Chromosome<RX, AX> bestChromosome) {
        this.currentBestValue = bestChromosome;
//        if (this.bestValues.size() >= 1000) {
//            this.bestValues = new ArrayList<>(this.bestValues.subList(1, 1000));
//        }
        this.bestValues.add(bestChromosome.getActualY());
        if (this.bestXLabel != null) {
            this.bestXLabel.setText(/*NumericUtils.toString(*/bestChromosome.getActualX().toString()/*)*/);
        }
        if (this.bestYLabel != null) {
            this.bestYLabel.setText(NumericUtils.toString(bestChromosome.getActualY()));
        }
    }

    public Chromosome<RX, AX> getCurrentBestValue() {
        return currentBestValue;
    }

    public List<Double> getBestValues() {
        return bestValues;
    }

    public List<Double> getAverageValues() {
        return averageValues;
    }

    public void addAverageValue(Double averageValue) {
        this.averageValues.add(averageValue);
    }

    public void setBestXLabel(WebLabel bestXLabel) {
        this.bestXLabel = bestXLabel;
    }

    public void setBestYLabel(WebLabel bestYLabel) {
        this.bestYLabel = bestYLabel;
    }

    public Tsp getTsp() {
        return tsp;
    }

    public void setTsp(Tsp tsp) {
        this.tsp = tsp;
    }

    public void setBestTourList(WebList bestTourList) {
        this.bestTourList = bestTourList;
    }

    public void setBestTourList(String[] cities) {
        this.bestTourList.setListData(cities);
    }

    public void setGenerateButton(WebButton generateButton) {
        this.generateButton = generateButton;
    }

    public void clickGenerateButton() {
        if (this.generateButton != null) {
            this.generateButton.doClick();
        }
    }
}

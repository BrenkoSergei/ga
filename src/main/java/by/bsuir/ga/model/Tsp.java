package by.bsuir.ga.model;

import java.util.ArrayList;
import java.util.List;

public class Tsp {

    private final String name;
    private final String comment;
    private final String type;
    private final int dimension;
    private final String edgeWeightType;
    private final List<City> cities = new ArrayList<>();

    public Tsp(String name, String comment, String type, int dimension, String edgeWeightType) {
        this.name = name;
        this.comment = comment;
        this.type = type;
        this.dimension = dimension;
        this.edgeWeightType = edgeWeightType;
    }

    public String getName() {
        return name;
    }

    public String getComment() {
        return comment;
    }

    public String getType() {
        return type;
    }

    public int getDimension() {
        return dimension;
    }

    public String getEdgeWeightType() {
        return edgeWeightType;
    }

    public List<City> getCities() {
        return cities;
    }

    public void addCity(City city) {
        cities.add(city);
    }
}
